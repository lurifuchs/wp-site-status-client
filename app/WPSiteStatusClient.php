<?php
/**
 * Plugin Name: WP Site Status Client
 * Description: Collects and pushes information about your site to your account on wpsitestatus.io.
 * Author: David Ajnered
 * Version: 1.0.0
 */

namespace App;

use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use \WP_REST_Response;

use \App\DataCollector;
use \App\DataTransporter;

class WPSiteStatusClient
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');

        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        // Setup or remove directories
        register_activation_hook(WPSSC_ROOT_FILE, [$this, 'wp_site_status_client_activate']);
        register_deactivation_hook(WPSSC_ROOT_FILE, [$this, 'wp_site_status_client_deactivate']);

        // Register API route
        add_action('rest_api_init', function() {
            include_once('routes.php');
        });

        // Admin menu
        if (is_multisite()) {
            add_action('network_admin_menu', [$this, 'wp_site_status_client_admin_network_menu']);

            // Form post callback
            add_action('network_admin_edit_wp_site_status_client', [$this, 'wp_site_status_client_save_settings']);

            // Missing token notice
            add_action('network_admin_notices', [$this, 'wp_site_status_client_missing_token_notice']);
        } else {
            add_action('admin_menu', [$this, 'wp_site_status_client_admin_menu']);

            // Missing token notice
            add_action('admin_notices', [$this, 'wp_site_status_client_missing_token_notice']);
        }


        // Form pre save hooks
        add_action('pre_update_option', function($value, $option_name, $old_value) {
            return $this->wp_site_status_client_pre_save($option_name, $value);
        }, 10, 3);

        // Form post save hooks
        add_action('updated_option', function($option_name, $old_value, $value) {
            return $this->wp_site_status_client_post_save($option_name, $value);
        }, 10, 3);
    }

    /**
     * Create directories on activation.
     *
     * @return void
     */
    function wp_site_status_client_activate()
    {
        mkdir(WPSSC_UPLOAD_PATH);
        mkdir(WPSSC_TEMP_UPLOAD_PATH);
        copy(WPSSC_PATH . 'assets/.gitignore.copy', WPSSC_UPLOAD_PATH . '.gitignore');
        copy(WPSSC_PATH . 'assets/index.php.copy', WPSSC_UPLOAD_PATH . 'index.php');
        copy(WPSSC_PATH . 'assets/index.php.copy', WPSSC_TEMP_UPLOAD_PATH . 'index.php');
    }

    /**
     * Cleanup on deactivation.
     *
     * @return void
     */
    function wp_site_status_client_deactivate()
    {
        try {
            $iterator = new RecursiveDirectoryIterator(WPSSC_UPLOAD_PATH, RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST);

            foreach ($files as $file) {
                if ($file->isDir()) {
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }

            rmdir(WPSSC_UPLOAD_PATH);
        } catch (\Exception $e) {
            // Probably nothing to remove
        }
    }

    /**
     * Add network option page for multisites.
     *
     * @return void
     */
    function wp_site_status_client_admin_network_menu()
    {
        add_submenu_page('settings.php', 'WP Site Status Client', 'WP Site Status Client', 'manage-options', 'wp_site_status_client', [$this, 'wp_site_status_client_admin_page']);
        add_action('admin_init', [$this, 'register_wp_site_status_client_settings']);
    }

    /**
     * Add option page.
     *
     * @return void
     */
    function wp_site_status_client_admin_menu()
    {
        add_options_page('WP Site Status Client', 'WP Site Status Client', 'manage_options', 'wp_site_status_client', [$this, 'wp_site_status_client_admin_page']);
        add_action('admin_init', [$this, 'register_wp_site_status_client_settings']);
    }

    /**
     * Register settings.
     *
     * @return void
     */
    function register_wp_site_status_client_settings()
    {
        register_setting('wp_site_status_client', 'wp_site_status_client_token', [
            'sanitize_callback' => [$this, 'wp_site_status_client_sanitize_token'],
        ]);

        register_setting('wp_site_status_client', 'wp_site_status_client_dev_mode');
        register_setting('wp_site_status_client', 'wp_site_status_client_filesystem');
        register_setting('wp_site_status_client', 'wp_site_status_client_filesystem_settings');
    }

    /**
     * Sanitize function
     *
     * @param string $token
     * @return void
     */
    function wp_site_status_client_sanitize_token($token)
    {
        $token = sanitize_text_field($token);
        $token = preg_replace("/[^a-zA-Z0-9]/", '', $token);

        if (strlen($token) !== 32) {
            add_settings_error('wp_site_status_client_token', 'wp-site-status-token-length', 'Token is too short', 'error');
        }

        return $token;
    }

    /**
     * Form post callback for multisite
     *
     * @return void
     */
    function wp_site_status_client_save_settings()
    {
        if (isset($_POST['wp_site_status_client_dev_mode'])) {
            update_site_option('wp_site_status_client_dev_mode', $_POST['wp_site_status_client_dev_mode']);
        }

        update_site_option('wp_site_status_client_filesystem', $_POST['wp_site_status_client_filesystem']);
        update_site_option('wp_site_status_client_filesystem_settings', $_POST['wp_site_status_client_filesystem_settings']);

        $token = $_POST['wp_site_status_client_token'];
        $token = $this->wp_site_status_client_sanitize_token($token);

        update_site_option('wp_site_status_client_token', $token);

        $this->collectAndSendData($token);

        wp_redirect(add_query_arg([
                'page' => 'wp_site_status_client',
                'updated' => true,
            ],
            network_admin_url('settings.php')
        ));

        exit;
    }

    /**
     * Pre save hook.
     *
     * @param string $old_value
     * @param string $new_value
     * @return void
     */
    public function wp_site_status_client_pre_save($option_name, $value)
    {
        return $value;
    }

    /**
     * Post save hook.
     *
     * @param string $old_value
     * @param string $new_value
     * @return void
     */
    public function wp_site_status_client_post_save($option_name, $value)
    {
        // Automatically push data when token is saved
        if ($option_name === 'wp_site_status_client_token') {
            $this->collectAndSendData($value);
        }
    }

    /**
     * Admin settings page.
     *
     * @return void
     */
    function wp_site_status_client_admin_page()
    {
        // Exposed variables in view
        $wpssc_dev_mode = get_site_option('wp_site_status_client_dev_mode');
        $wpssc_token = get_site_option('wp_site_status_client_token');
        $wpssc_filesystem = get_site_option('wp_site_status_client_filesystem');
        $wpssc_filesystem_settings = get_site_option('wp_site_status_client_filesystem_settings');

        $filesystemConfig = \App\Backup\Config\FilesystemConfig::fromSetting();

        include(WPSSC_PATH . '/views/option-page.php');
    }

    /**
     * Show notification if plugin is not configured.
     *
     * @return void
     */
    function wp_site_status_client_missing_token_notice()
    {
        if (isset($_GET['connect']) && $_GET['connect'] === 'success'): ?>
            <div class="notice notice-success is-dismissible">
                <p>Your site is now connected to WP Site Status.</p>
            </div>
        <?php endif;

        $token = get_site_option('wp_site_status_client_token');

        if (!$token || empty($token)): ?>
            <div class="notice notice-warning is-dismissible">
                <p><b>WP Site Status Client:</b> Client token is missing.</p>
            </div>
        <?php endif;
    }

    /**
     * Route callback: Collect data and send to server.
     */
    public function wp_site_status_client_router_update_callback()
    {
        $token = get_site_option('wp_site_status_client_token');

        if (!$token) {
            return ['error' => 'token is missing'];
        }

        $response = $this->collectAndSendData($token);

        if ($_GET['redirect']) {
            $_GET['redirect'] .= '&connect=success';
            wp_redirect($_GET['redirect']);
        } else {
            return new WP_REST_Response($response['message'], $response['code']);
            // return $response;
        }
    }

    /**
     * Route callback: Backup db.
     *
     * @return void
     */
    public function wp_site_status_client_router_backup_callback()
    {
        $database = new \App\Backup\Databases\MySQLDatabase(\App\Backup\Config\DatabaseConfig::fromSetting());

        $filesystemConfig = \App\Backup\Config\FilesystemConfig::fromSetting();
        $filesystem = \App\Backup\Filesystems\FilesystemFactory::create($filesystemConfig);
        $procedure = new \App\Backup\BackupProcedure($database, $filesystem);

        $response = $procedure->run();

        return new WP_REST_Response($response['message'], $response['code']);
    }

    /**
     * Route callback: Revoke dropbox access
     *
     * @return void
     */
    public function wp_site_status_client_router_dropbox_revoke_callback()
    {
        $database = new \App\Backup\Databases\MySQLDatabase(\App\Backup\Config\DatabaseConfig::fromSetting());

        $filesystemConfig = \App\Backup\Config\FilesystemConfig::fromSetting();
        $filesystem = \App\Backup\Filesystems\FilesystemFactory::create($filesystemConfig);
        $filesystem->revokeAccessToken();
        update_site_option('wp_site_status_client_filesystem_settings', []);

        wp_redirect(wp_get_referer());
    }

    /**
     * Collect all data and send to server.
     *
     * @param string $token
     * @return void
     */
    private function collectAndSendData($token)
    {
        $collector = new DataCollector();
        $transporter = new DataTransporter();
        $data = $collector->get();
        return $transporter->send($data, $token);
    }
}
