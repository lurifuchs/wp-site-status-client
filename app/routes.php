<?php

register_rest_route('wp-site-status-client', '/update', [
    'methods' => 'GET',
    'callback' => [$this, 'wp_site_status_client_router_update_callback'],
]);

register_rest_route('wp-site-status-client', '/backup', [
    'methods' => 'GET',
    'callback' => [$this, 'wp_site_status_client_router_backup_callback'],
]);

register_rest_route('wp-site-status-client', '/dropbox/revoke', [
    'methods' => 'GET',
    'callback' => [$this, 'wp_site_status_client_router_dropbox_revoke_callback'],
]);