<?php

namespace App;

class DataCollector
{
    /**
     * Collect data for sites and plugins.
     *
     * @return array
     */
    public function get()
    {
        $data = [];

        // Normalize format between single sites and multisites
        if (is_multisite()) {
            $data = wp_get_sites();
        } else {
            $data[] = ['blog_id' => 1]; // Set one for non-mu sites
        }

        $plugins = [];
        foreach ($data as $index => $site) {
            // Set default blog
            $this->switchBlog($site['blog_id']);

            // Get information about the site and it's active plugins
            $site['name'] = get_bloginfo('name');
            $site['slug'] = $this->url_as_slug(get_bloginfo('url'));
            $site['url'] = get_bloginfo('url');
            $site['version'] = get_bloginfo('version');
            $site['php_version'] = phpversion();
            $site['multisite'] = is_multisite();
            $site['main_site'] = 0;

            if (is_multisite()) {
                $site['main_site'] = ($site['blog_id'] == BLOG_ID_CURRENT_SITE) ? 1 : 0;
            }

            // Get active plugins for current site in loop
            $activePlugins = $this->getActivePlugins();

            if (empty($plugins)) {
                // Loop through all the plugins in /wp-content/plugins and collect information
                foreach (get_plugins() as $path => $plugin) {
                    $pluginSlug = substr($path, 0, strpos($path, '/'));

                    $plugin = [
                        'name' => $plugin['Name'],
                        'version' => $plugin['Version'],
                        'slug' => $pluginSlug,
                        'path' => $path,
                    ];

                    // Is plugin active
                    $isActive = in_array($path, $activePlugins) ? 1 : 0;
                    $isNetworkActive = is_plugin_active_for_network($path);
                    $site['active_plugins'][$plugin['path']]['active'] = $isActive;
                    $site['active_plugins'][$plugin['path']]['network_active'] = $isNetworkActive;
                    $plugins[] = $plugin;
                }
            } else {
                foreach ($plugins as $plugin) {
                    $isActive = in_array($plugin['path'], $activePlugins) ? true : false;
                    $isNetworkActive = is_plugin_active_for_network($plugin['path']);
                    $site['active_plugins'][$plugin['path']]['active'] = $isActive;
                    $site['active_plugins'][$plugin['path']]['network_active'] = $isNetworkActive;
                }
            }

            $data[$index] = $site;
            $data[$index]['plugins'] = $plugins;
        }

        $this->restoreCurrentBlog();

        return $data;
    }

    /**
     * Convert URL to a nice slug.
     *
     * @param string $url
     * @return string
     */
    private function url_as_slug($url)
    {
        $find = array('http://www.', 'https://www.', 'http://', 'https://');
        return sanitize_title(str_replace($find, '', $url));
    }

    /**
     * Helper function.
     *
     * @var int $blogId
     */
    private function switchBlog($blogId)
    {
        if (is_multisite()) {
            switch_to_blog($blogId);
        }
    }

    /**
     * Helper function.
     */
    private function restoreCurrentBlog()
    {
        if (is_multisite()) {
            restore_current_blog();
        }
    }

    /**
     * Get active plugins for a site.
     *
     * Should only be called from getDataForBlog or other function that actively selects the current blog.
     * Could result in errors if used directly.
     *
     * @return array
     */
    private function getActivePlugins()
    {
        $plugins = [];
        $activePlugins = get_option('active_plugins');
        if ($activePlugins) {
            foreach ($activePlugins as $path) {
                $plugins[] = $path;
            }
        }

        return $plugins;
    }
}