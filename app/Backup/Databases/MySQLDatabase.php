<?php

namespace App\Backup\Databases;

use Ifsnop\Mysqldump as IMysqldump;

class MySQLDatabase extends Database
{
    /**
     * Dump database.
     *
     * @return string filepath
     */
    public function dump()
    {
        try {
            $connectionString = $this->getConnectionString();
            $dump = new IMysqldump\Mysqldump($connectionString, $this->config->get('DB_USER'), $this->config->get('DB_PASSWORD'));
            $filePath = WPSSC_TEMP_UPLOAD_PATH . $this->getFilename();
            $dump->start($filePath);
            return $filePath;
        } catch (\Exception $e) {
            error_log('failed');
            error_log(var_export($e->getMessage(), true));
            return $e->getMessage();
        }
    }

    /**
     * Restore database. Not yet implemented.
     *
     * @param string $filePath
     * @return void
     */
    public function restore($filePath)
    {

    }

    /**
     * Get MySQL connection string from config.
     *
     * @return string
     */
    private function getConnectionString()
    {
        return 'mysql:host=' . $this->config->get('DB_HOST') . ';dbname=' . $this->config->get('DB_NAME');
    }
}