<?php

namespace App\Backup\Filesystems;

use App\Backup\Config\Config;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\DropboxFile;

class DropboxFilesystem extends Filesystem
{
    /**
     * @var Dropbox
     */
    private $dropbox;

    /**
     * Constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        parent::__construct($config);

        $app = new DropboxApp(null, null, $this->config->get('access_token'));
        $this->dropbox = new Dropbox($app);
    }

    /**
     * Move temporary backup file to storage.
     *
     * @param string $filePath
     * @return void
     */
    public function move($filePath)
    {
        try {
            $chunkSize = 100000000; // 100mb in bytes
            $dropboxFile = new DropboxFile($filePath);
            $fileMetadata = $this->dropbox->uploadChunked($dropboxFile, '/' . $dropboxFile->getFileName(), $dropboxFile->getSize(), $chunkSize, ['autorename' => true]);

            return [
                'code' => 200,
                'message' => 'Success'
            ];
        } catch (\Exception $e) {
            // Remove file if upload crashes
            $this->delete($filePath);

            return [
                'code' => 400,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * Remove all files in temp.
     *
     * @return void
     */
    public function deleteTempFiles($filePath)
    {
        $files = scandir($filePath);
        foreach ($files as $file) {
            if (!in_array($file, ['.', '..'])) {
                $this->delete($filePath . $file);
            }
        }
    }

    /**
     * Delete temporary backup file.
     *
     * @param string $filePath
     * @return void
     */
    public function delete($filePath)
    {
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }

    /**
     * Revoke access to dropbox
     *
     * @return bool
     */
    public function revokeAccessToken()
    {
        $authHelper = $this->dropbox->getAuthHelper();
        $authHelper->revokeAccessToken();
    }
}