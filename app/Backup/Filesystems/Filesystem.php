<?php

namespace App\Backup\Filesystems;

use App\Backup\Config\Config;

abstract class Filesystem
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * Constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Move temporary backup file to storage.
     *
     * @param string $filePath
     * @return void
     */
    public abstract function move($filePath);

    /**
     * Delete temporary backup file.
     *
     * @param string $filePath
     * @return void
     */
    public abstract function delete($filePath);
}