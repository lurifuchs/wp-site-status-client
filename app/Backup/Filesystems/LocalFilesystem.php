<?php

namespace App\Backup\Filesystems;

class LocalFilesystem extends Filesystem
{
    /**
     * Move temporary backup file to storage.
     *
     * @param [type] $filePath
     * @return void
     */
    public function move($filePath)
    {
        copy($filePath, WPSSC_BACKUP_UPLOAD_PATH . '/' . basename($filePath));
    }

    /**
     * Delete temporary backup.
     *
     * @param [type] $filePath
     * @return void
     */
    public function delete($filePath)
    {
        unlink($filePath);
    }
}