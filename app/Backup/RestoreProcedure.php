<?php

namespace App\Backup;

use App\Backup\Databases\Database;
use App\Backup\Filesystems\Filesystem;

class RestoreProcedure
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @var Filesystem
     */
    private $filesytem;

    /**
     * Constructor.
     *
     * @param Database $database
     * @param Filesystem $filesystem
     */
    public function __construct(Database $database, Filesystem $filesystem)
    {
        $this->database = $database;
        $this->filesystem = $filesystem;
    }

    /**
     * Run restore procedure.
     *
     * @return void
     */
    public function run()
    {
        // Restore procedure
    }
}