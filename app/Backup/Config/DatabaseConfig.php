<?php

namespace App\Backup\Config;

require(ABSPATH . '/wp-config.php');

class DatabaseConfig extends Config
{
    /**
     * Create config from wp-config.php variables.
     *
     * @return DatabaseConfig
     */
    public static function fromSetting()
    {
        return new self([
            'DB_NAME' => DB_NAME,
            'DB_USER' => DB_USER,
            'DB_PASSWORD' => DB_PASSWORD,
            'DB_HOST' => DB_HOST,
            'DB_CHARSET' => DB_CHARSET,
            'DB_COLLATE' => DB_COLLATE,
        ]);
    }
}
