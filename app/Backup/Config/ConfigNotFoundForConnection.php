<?php

namespace App\Backup\Config;

/**
 * Class ConfigNotFoundForConnection.
 */
class ConfigNotFoundForConnection extends \Exception
{
}
