<?php

namespace App\Backup\Config;

class Config
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * Create config from settings.
     *
     * @return Config
     */
    public static function fromSetting()
    {
    }

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Get config value.
     *
     * @param $name
     * @param null $field
     *
     * @throws ConfigFieldNotFound
     * @throws ConfigNotFoundForConnection
     *
     * @return mixed
     */
    public function get($name, $field = null)
    {
        if (!array_key_exists($name, $this->config)) {
            return null;
            // throw new ConfigNotFoundForConnection("Could not find configuration for connection {$name}");
        }
        if ($field) {
            return $this->getConfigField($name, $field);
        }

        return $this->config[$name];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->config;
    }

    /**
     * @param $name
     * @param $field
     *
     * @throws ConfigFieldNotFound
     *
     * @return mixed
     */
    private function getConfigField($name, $field)
    {
        if (!array_key_exists($field, $this->config[$name])) {
            throw new ConfigFieldNotFound("Could not find field {$field} in configuration for connection type {$name}");
        }

        return $this->config[$name][$field];
    }
}
